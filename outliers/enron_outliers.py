#!/usr/bin/python

import pickle
import sys
import matplotlib.pyplot
sys.path.append("../tools/")
from feature_format import featureFormat, targetFeatureSplit


### read in data dictionary, convert to numpy array
data_dict = pickle.load( open("../final_project/final_project_dataset.pkl", "r") )

max_salary_1 = 0
max_salary_1_key = ''
for data_element in data_dict:
    # print data_element[0]['salary']
    if data_dict[data_element]['salary'] != 'NaN' and data_dict[data_element]['salary'] > max_salary_1:
        max_salary_1 = data_dict[data_element]['salary']
        max_salary_1_key = data_element

print max_salary_1_key
print max_salary_1
print(data_dict[max_salary_1_key])

data_dict.pop( "TOTAL", 0 )
data_dict.pop( "LAY KENNETH L", 0 )
data_dict.pop( "SKILLING JEFFREY K", 0 )

features = ["salary", "bonus"]
data = featureFormat(data_dict, features)

for name in data_dict:
    # float() does not include NaN values
    bonus = float(data_dict[name]["bonus"])
    salary = float(data_dict[name]["salary"])
    if bonus >= 5000000 and salary >= 1000000:
        print name, "bonus: ", data_dict[name]["bonus"], "salary: ", data_dict[name]["salary"]

### your code below
for point in data:
    salary = point[0]
    bonus = point[1]
    matplotlib.pyplot.scatter( salary, bonus )



matplotlib.pyplot.xlabel("salary")
matplotlib.pyplot.ylabel("bonus")
matplotlib.pyplot.show()


