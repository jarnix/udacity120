#!/usr/bin/python

""" 
    Starter code for exploring the Enron dataset (emails + finances);
    loads up the dataset (pickled dict of dicts).

    The dataset has the form:
    enron_data["LASTNAME FIRSTNAME MIDDLEINITIAL"] = { features_dict }

    {features_dict} is a dictionary of features associated with that person.
    You should explore features_dict as part of the mini-project,
    but here's an example to get you started:

    enron_data["SKILLING JEFFREY K"]["bonus"] = 5600000
    
"""
import sys
import numpy as np
sys.path.append("../tools/")
from feature_format import featureFormat
from feature_format import targetFeatureSplit

import pickle

enron_data = pickle.load(open("../final_project/final_project_dataset.pkl", "r"))

sum = 0
for i in enron_data:
    if enron_data[i]['poi']:
        sum+=1
        #print enron_data[i]
        
print "how many poi", sum

print "total stock of James Prentice", enron_data["PRENTICE JAMES"]["total_stock_value"]
print "email from Wesley Colwell to POIs", enron_data["COLWELL WESLEY"]["from_this_person_to_poi"]
print "stock exercised by Jeffrey S Skilling", enron_data["SKILLING JEFFREY K"]["exercised_stock_options"]

for i in enron_data:
    if enron_data[i]['poi']:
        print "i", i, "total payments", enron_data[i]["total_payments"]        
#        print 

print "Skilling payments", enron_data["SKILLING JEFFREY K"]["total_payments"]
print "Fastow payments", enron_data["FASTOW ANDREW S"]["total_payments"]
print "Lay payments", enron_data["LAY KENNETH L"]["total_payments"]

nbWithSalary = 0
nbWithEmail = 0
for i in enron_data:
    if enron_data[i]['salary'] != "NaN":
        print "i", i, "salary", enron_data[i]["salary"]   
        nbWithSalary += 1
    if enron_data[i]['email_address'] != "NaN":
        print "i", i, "email_address", enron_data[i]["email_address"]   
        nbWithEmail += 1

print "nb with salary", nbWithSalary
print "nb with email", nbWithEmail

features_list = ["bonus", "salary"]
data = featureFormat( enron_data, features_list, remove_any_zeroes=True)
target, features = targetFeatureSplit( data )