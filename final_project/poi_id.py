#!/usr/bin/python

# https://discussions.udacity.com/t/final-project-sharing-my-code-and-results/169287

import sys
import pickle
import math
import matplotlib.pyplot
sys.path.append("../tools/")

from feature_format import featureFormat, targetFeatureSplit
from tester import dump_classifier_and_data

### Task 1: Select what features you'll use.
### features_list is a list of strings, each of which is a feature name.
### The first feature must be "poi".
features_list = ['poi','salary','bonus','total_payments','total_stock_value','long_term_incentive', 'from_poi_to_this_person','from_messages','from_this_person_to_poi','to_messages'] # You will need to use more features

### Load the dictionary containing the dataset
with open("final_project_dataset.pkl", "r") as data_file:
    data_dict = pickle.load(data_file)

### Task 2: Remove outliers
data_dict.pop( "TOTAL", 0 )

for point in data_dict:
    if data_dict[point]['salary'] != 'NaN' and data_dict[point]['bonus'] != 'NaN':
        salary = data_dict[point]['salary']
        bonus = data_dict[point]['bonus']
        matplotlib.pyplot.scatter( salary, bonus )

matplotlib.pyplot.xlabel("salary")
matplotlib.pyplot.ylabel("bonus")
matplotlib.pyplot.show()

for name in data_dict:
    # float() does not include NaN values
    bonus = float(data_dict[name]["bonus"])
    salary = float(data_dict[name]["salary"])
    if bonus >= 4000000 or salary >= 1000000:
        print name, "bonus: ", data_dict[name]["bonus"], "salary: ", data_dict[name]["salary"]
        
data_dict.pop("LAY KENNETH L", 0)
data_dict.pop("SKILLING JEFFREY K", 0)
data_dict.pop("BELDEN TIMOTHY N", 0)
data_dict.pop("ALLEN PHILLIP K", 0)
data_dict.pop("FREVERT MARK A", 0)
data_dict.pop("LAVORATO JOHN J", 0)

for point in data_dict:
    if data_dict[point]['salary'] != 'NaN' and data_dict[point]['bonus'] != 'NaN':
        salary = data_dict[point]['salary']
        bonus = data_dict[point]['bonus']
        matplotlib.pyplot.scatter( salary, bonus )

matplotlib.pyplot.xlabel("salary")
matplotlib.pyplot.ylabel("bonus")
matplotlib.pyplot.show()

#### Task 3: Create new feature(s)
keys = data_dict.keys()
for key in keys:
    to_poi_ratio = float(data_dict[key]['from_this_person_to_poi']) / float(data_dict[key]['from_messages'])
    from_poi_ratio = float(data_dict[key]['from_poi_to_this_person']) / float(data_dict[key]['to_messages'])
    data_dict[key]['to_poi_ratio'] = "NaN" if math.isnan(to_poi_ratio) else to_poi_ratio
    data_dict[key]['from_poi_ratio'] = "NaN" if math.isnan(from_poi_ratio) else from_poi_ratio

### Store to my_dataset for easy export below.
my_dataset = data_dict

### Extract features and labels from dataset for local testing
data = featureFormat(my_dataset, features_list, sort_keys = True)
labels, features = targetFeatureSplit(data)

print features

### Task 4: Try a varity of classifiers
### Please name your classifier clf for easy export below.
### Note that if you want to do PCA or other multi-stage operations,
### you'll need to use Pipelines. For more info:
### http://scikit-learn.org/stable/modules/pipeline.html

# Provided to give you a starting point. Try a variety of classifiers.
from sklearn.naive_bayes import GaussianNB
clf = GaussianNB()

### Task 5: Tune your classifier to achieve better than .3 precision and recall 
### using our testing script. Check the tester.py script in the final project
### folder for details on the evaluation method, especially the test_classifier
### function. Because of the small size of the dataset, the script uses
### stratified shuffle split cross validation. For more info: 
### http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.StratifiedShuffleSplit.html

# Example starting point. Try investigating other evaluation techniques!
from sklearn.cross_validation import train_test_split
features_train, features_test, labels_train, labels_test = \
    train_test_split(features, labels, test_size=0.3, random_state=42)

### Task 6: Dump your classifier, dataset, and features_list so anyone can
### check your results. You do not need to change anything below, but make sure
### that the version of poi_id.py that you submit can be run on its own and
### generates the necessary .pkl files for validating your results.

dump_classifier_and_data(clf, my_dataset, features_list)