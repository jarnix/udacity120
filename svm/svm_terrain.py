#!/usr/bin/python

""" 
    This is the code to accompany the Lesson 2 (SVM) mini-project.

    Use a SVM to identify emails from the Enron corpus by their authors:    
    Sara has label 0
    Chris has label 1
"""
    
import sys
sys.path.append("../tools/")
from class_vis import prettyPicture
from prep_terrain_data import makeTerrainData

import matplotlib.pyplot as plt
import copy
import numpy as np
import pylab as pl

import time

features_train, labels_train, features_test, labels_test = makeTerrainData()

########################## SVM #################################
### we handle the import statement and SVC creation for you here
from sklearn.svm import SVC
clf = SVC(kernel="rbf", C=10, gamma=10)


#### now your job is to fit the classifier
#### using the training features/labels, and to
#### make a set of predictions on the test data

#t0 = time()

clf.fit(features_train, labels_train)
pred = clf.predict(features_test)

#print "training time:", round(time()-t0, 3), "s"

#### store your predictions in a list named pred


from sklearn.metrics import accuracy_score
acc = accuracy_score(pred, labels_test)
print acc

prettyPicture(clf, features_test, labels_test)

def submitAccuracy():
    return acc